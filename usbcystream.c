#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/scatterlist.h>
#include <linux/mutex.h>
#include <linux/timer.h>
#include <linux/usb.h>


#define CYSTREAM_MINOR 64

#define INBUF_SIZE 0x1000
#define OUTBUF_SIZE 0x1000

#define USB_CONCURRENT_WRITE 1
struct cystream_endpoint{
       unsigned char *bulk_buffer;
       size_t bulk_size;
       __u8   bulk_endpointAddr;
};

struct cystream_usbdev{
//       struct mutex lock;	      /* general lock */  
       struct usb_device *cysdev;
       struct usb_interface *interface;  /* Interface representation */

       struct kref kref;

       struct cystream_endpoint outCyEp[2];
       struct cystream_endpoint inCyEp[2];

       struct semaphore rwsem;
       
};


static struct usb_driver cystream_driver;

//static DEFINE_MUTEX(cystream_mutex);
//static struct cystream_usbdev cystream_instance;

//#define USBTEST_REQUEST	_IOWR('U', 100, struct usbtest_param)


static struct usb_driver cystream_driver;

static void cystream_cleanup(struct kref *kref)
{ 
        struct cystream_usbdev *udev = container_of(kref, 
                 struct  cystream_usbdev, kref);
        usb_put_dev(udev->cysdev);
        kfree(udev->bulk_in_buffer);
        kfree(udev);
}

static ssize_t
read_cystream(struct file *file, char __user *buffer, size_t count, loff_t * ppos)
{
        struct cystream_usbdev *udev;
        int retval = 0;
        int bytes_read;
        
        printk(KERN_DEBUG "read cystream is called");

        udev = file->private_data;
        retval = usb_bulk_msg(udev->cysdev,
			      usb_rcvbulkpipe(udev->cysdev, 
                                              udev->bulk_in_endpointAddr),
                              udev->bulk_in_buffer,
                              min(udev->bulk_in_size, count),
                              &bytes_read, 10000);
        if (!retval) {
               if(copy_to_user(buffer, udev->bulk_in_buffer, bytes_read))
                      retval = -EFAULT;
               else 
                      retval = bytes_read;
        }
	return retval;
        
}

static ssize_t
write_cystream(struct file *file, const char __user *buffer, size_t count, loff_t * ppos)
{
        
	return -EIO;
}

static long ioctl_cystream(struct file *file, unsigned int cmd, unsigned long arg)
{
	return -EIO;
}




static int close_cystream(struct inode *inode, struct file *file)
{
       struct cystream_usbdev *udev;
       udev = file->private_data;
       if (udev == NULL) return -ENODEV;
       usb_autopm_put_interface(udev->interface);
       kref_put(&udev->kref, cystream_cleanup);
       return 0;
}



static int open_cystream(struct inode *inode, struct file *file)
{
        struct cystream_usbdev *udev;
        struct usb_interface *interface;
        int subminor = iminor(inode);
        int r;
       
        printk(KERN_INFO "open_cystream is called. \n");

        interface = usb_find_interface(&cystream_driver, subminor);


        if(!interface){ 
            printk(KERN_ERR "USB CYSTREAM %s -error, can't find device for minor %d\n",
                        __func__, subminor);
            return -ENODEV;
        }
        udev = usb_get_intfdata(interface);
        
        if (!udev) {
            printk(KERN_INFO " can not get usb intfdata");
            return -ENODEV;
        }
        
        /* increase usage counter for this device */
        kref_get(&udev->kref);
      
#if 0  
        printk(KERN_INFO "open_cystream interface is got>. \n");
        r = usb_autopm_get_interface(interface);

        printk(KERN_INFO "open_cystream interface is got<. %d \n", r);

        if ( r < 0) {
              kref_put(&udev->kref, cystream_cleanup);
              printk(KERN_INFO "can not get usb autopm interface");
              return r;
        }  
#endif 
 
        file->private_data = udev;
        printk(KERN_INFO "open_cystream is done. \n");
	return 0;
}

static struct file_operations usb_cystream_fops = {
        .owner =        THIS_MODULE,
        .read =         read_cystream,
        .write =        write_cystream,
        .unlocked_ioctl = ioctl_cystream,
        .open =         open_cystream,
        .release =      close_cystream,
        .llseek =       noop_llseek,
};


static struct usb_class_driver usb_cystream_class = {
        .name =      "cystream",
        .fops =       &usb_cystream_fops,
        .minor_base =   CYSTREAM_MINOR,
};


static int
cystream_probe(struct usb_interface *intf, const struct usb_device_id *id)
{

        struct cystream_usbdev *udev = NULL;
        struct usb_host_interface *iface_desc;
        struct usb_endpoint_descriptor *endpoint;
        int retval = -ENOMEM;
        int i;

        udev = kzalloc(sizeof(struct cystream_usbdev), GFP_KERNEL);
        if (udev == NULL) {
             dev_err(&intf->dev, "Out of memory\n");
             return -ENOMEM;
        }
        
        kref_init(&udev->kref);
        sema_init(&udev->rwsem, USB_CONCURRENT_WRITE);
        
        udev->cysdev = usb_get_dev(interface_to_usbdev(intf));
#if 0
        i = usb_set_interface(udev->cysdev, intf->altsetting->desc.bInterfaceNumber,2);
        dev_info(&intf->dev, "set interface result %d\n", i);
#endif

        udev->interface = intf;
        dev_info(&intf->dev, "USB Cystream found at address %d and with endpoint numbers %d in interface.  \n", udev->cysdev->devnum, 
                              intf->altsetting->desc.bNumEndpoints);

        iface_desc = intf -> altsetting;
        int iep = 0, outep = 0;
        for (i=0; i< iface_desc->desc.bNumEndpoints; i++) {
              endpoint = &iface_desc->endpoint[i].desc;

              if (!udev->bulk_in_endpointAddr && 
                   usb_endpoint_is_bulk_in(endpoint)) {
                       udev->bulk_in_size = usb_endpoint_maxp(endpoint);
                       udev->bulk_in_endpointAddr = endpoint->bEndpointAddress;

                       dev_info(&intf->dev, "USB Cystream endpoint address %ld, %2x\n", 
                                udev->bulk_in_size, udev->bulk_in_endpointAddr);

                       udev->bulk_in_buffer = kmalloc(udev->bulk_in_size, GFP_KERNEL);
                       if ( !udev->bulk_in_buffer) {
                              dev_err(&intf->dev,
                                      "Could not alloc bulk in buffer \n"); 
                              goto error;
                       }
              }
               
              if ( !udev->bulk_out_endpointAddr && 
                   usb_endpoint_is_bulk_out(endpoint)) {
                   udev->bulk_out_endpointAddr = endpoint->bEndpointAddress;
              }    
        }

	usb_set_intfdata(intf, udev);
        retval = usb_register_dev(intf, &usb_cystream_class);
        if (retval) {
                dev_err(&intf->dev,
                        "Not able to get a minor for this device.\n");
                goto error;
        }
	return 0;
error: 
	if(udev)
            kref_put(&udev->kref, cystream_cleanup);
        return retval; 
}


static int cystream_suspend(struct usb_interface *intf, pm_message_t message)
{
	return 0;
}

static int cystream_resume(struct usb_interface *intf)
{
	return 0;
}


static void cystream_disconnect(struct usb_interface *intf)
{
	struct cystream_usbdev *udev;
        int minor = intf -> minor;

        udev = usb_get_intfdata(intf);
	usb_set_intfdata(intf, NULL);
        usb_deregister_dev(intf, &usb_cystream_class);
        kref_put(&udev->kref, cystream_cleanup);
        dev_info(&intf->dev, "USB CyStream #%d now disconnected\n", minor);

}

/* fx2 version of ez-usb */
#if 0
 
static struct cystream_info cypress_info = {
	.name		= "CyStream device",
	.ep_in		= 6,
	.ep_out		= 2,
	.alt		= 1,
};
#endif


static const struct usb_device_id id_table[] = {

	/* generic EZ-USB FX2 controller CyStream (or development board) */
	{ USB_DEVICE(0x04b4, 0x1004),
//      	.driver_info = (unsigned long) &cypress_info,
	},
	{ }
};
MODULE_DEVICE_TABLE(usb, id_table);

static struct usb_driver cystream_driver = {
	.name =		"CyStream",
	.id_table =	id_table,
	.probe =	cystream_probe,
	.disconnect =	cystream_disconnect,
	.suspend =	cystream_suspend,
	.resume =	cystream_resume,
};

/*-------------------------------------------------------------------------*/

static int __init cystream_init(void)
{
	return usb_register(&cystream_driver);
}
module_init(cystream_init);

static void __exit cystream_exit(void)
{
	usb_deregister(&cystream_driver);
}

module_exit(cystream_exit);

MODULE_DESCRIPTION("USB Core/HCD CYPRESS CYSTREAM Testing Driver");
MODULE_LICENSE("GPL");

