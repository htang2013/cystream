#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/scatterlist.h>
#include <linux/mutex.h>
#include <linux/timer.h>
#include <linux/uaccess.h>
#include <linux/usb.h>


#define CYSTREAM_MINOR 64

#define INBUF_SIZE 0x1000
#define OUTBUF_SIZE 0x1000

#define USB_CONCURRENT_WRITE 1


struct bulkloop_usbdev{
//       struct mutex lock;	      /* general lock */  
       struct usb_device *cysdev;
       struct usb_interface *interface;  /* Interface representation */

       struct kref kref;
       size_t bulk_in_size;

       unsigned char *bulk_in_buffer;

       // two ep in and ep out forming two loops.
       struct {
           __u8   bulk_endpointAddr;
           size_t bulk_size;
           unsigned char *bulk_buffer;      
       } EpIn[2], EpOut[2]; 
       

       struct semaphore rwsem;
       struct usb_anchor submitted;        
};


static struct usb_driver bulkloop_driver;

//static DEFINE_MUTEX(bulkloop_mutex);
//static struct bulkloop_usbdev bulkloop_instance;

//#define USBTEST_REQUEST	_IOWR('U', 100, struct usbtest_param)


static struct usb_driver bulkloop_driver;

static void bulkloop_cleanup(struct kref *kref)
{ 
        struct bulkloop_usbdev *udev = container_of(kref, 
                 struct  bulkloop_usbdev, kref);
        usb_put_dev(udev->cysdev);
        kfree(udev->bulk_in_buffer);
        kfree(udev);
}

static ssize_t
read_bulkloop(struct file *file, char __user *buffer, size_t count, loff_t * ppos)
{
        struct bulkloop_usbdev *udev;
        int retval = 0;
        int bytes_read;
        
        printk(KERN_DEBUG "read bulkloop is called");

        udev = file->private_data;

        retval = usb_bulk_msg(udev->cysdev,
			      usb_rcvbulkpipe(udev->cysdev, 
                                              udev->EpIn[0].bulk_endpointAddr),
                              udev->EpIn[0].bulk_buffer,
                              min(udev->EpIn[0].bulk_size, count),
                              &bytes_read, 10000);
        if (!retval) {
               if(copy_to_user(buffer, udev->EpIn[0].bulk_buffer, bytes_read))
                      retval = -EFAULT;
               else 
                      retval = bytes_read;
        }

	return retval;
        
}


static void bulkloop_write_callback(struct urb *urb)
{
      struct bulkloop_usbdev *udev;
      int status = urb->status;
      udev = urb->context;
      if(status &&
         !(status == -ENOENT ||
           status == -ECONNRESET ||
           status == -ESHUTDOWN))
      {
         dev_dbg(&udev->interface->dev,
                 "nonzero write bulk status received: %d\n", status);
      }
      usb_free_coherent(urb->dev, urb->transfer_buffer_length,
                        urb->transfer_buffer, urb->transfer_dma);
      up(&udev->rwsem); 
      
}


static ssize_t
write_bulkloop(struct file *file, const char __user *buffer, size_t count, loff_t * ppos)
{
        struct bulkloop_usbdev *udev = file->private_data;
        struct urb *urb = NULL;
        char *buf = NULL;
        int retval = 0, r;

        if ( count == 0 ) return 0;

        r = down_interruptible(&udev->rwsem);
        if (r< 0)
                 return -EINTR;
	urb = usb_alloc_urb(0, GFP_KERNEL);
        if (!urb) {
                retval = -ENOMEM;
                //release urb
	        goto err_no_buf;
         }
         buf = usb_alloc_coherent(udev->cysdev, count, GFP_KERNEL,
                                  &urb->transfer_dma);
         if (!buf) {
               retval = -ENOMEM;
               //release buf;
               goto error;
          }
          
          if (copy_from_user(buf, buffer, count)) {
              retval = -EFAULT;
              return retval;
          }
           
          usb_fill_bulk_urb(urb,udev->cysdev,
                            usb_sndbulkpipe(udev->cysdev,
                                 udev->EpOut[0].bulk_endpointAddr),
                            buf, count, bulkloop_write_callback, udev);
          urb->transfer_flags |= URB_NO_TRANSFER_DMA_MAP;
          usb_anchor_urb(urb , &udev->submitted);        
          retval = usb_submit_urb(urb, GFP_KERNEL);
          if (retval) {
               dev_err(&udev->cysdev->dev,
                       "%s - failed submitting write urb, error %d\n",
                       __func__, retval);
               goto error_unanchor;
          }
          usb_free_urb(urb);

          return count;      

error_unanchor:
         usb_unanchor_urb(urb);

error:
         usb_free_coherent(udev->cysdev, count , buf, urb->transfer_dma);
         usb_free_urb(urb);
err_no_buf:
         up(&udev->rwsem);
         return retval;	
}

static long ioctl_bulkloop(struct file *file, unsigned int cmd, unsigned long arg)
{
	return -EIO;
}




static int close_bulkloop(struct inode *inode, struct file *file)
{
       struct bulkloop_usbdev *udev;
       udev = file->private_data;
       if (udev == NULL) return -ENODEV;
       usb_autopm_put_interface(udev->interface);
       kref_put(&udev->kref, bulkloop_cleanup);
       return 0;
}



static int open_bulkloop(struct inode *inode, struct file *file)
{
        struct bulkloop_usbdev *udev;
        struct usb_interface *interface;
        int subminor = iminor(inode);
       
        printk(KERN_INFO "open_bulkloop is called. \n");

        interface = usb_find_interface(&bulkloop_driver, subminor);


        if(!interface){ 
            printk(KERN_ERR "USB CYSTREAM %s -error, can't find device for minor %d\n",
                        __func__, subminor);
            return -ENODEV;
        }
        udev = usb_get_intfdata(interface);
        
        if (!udev) {
            printk(KERN_INFO " can not get usb intfdata");
            return -ENODEV;
        }
        
        /* increase usage counter for this device */
        kref_get(&udev->kref);
      
#if 0  
        printk(KERN_INFO "open_bulkloop interface is got>. \n");
        r = usb_autopm_get_interface(interface);

        printk(KERN_INFO "open_bulkloop interface is got<. %d \n", r);

        if ( r < 0) {
              kref_put(&udev->kref, bulkloop_cleanup);
              printk(KERN_INFO "can not get usb autopm interface");
              return r;
        }  
#endif 
 
        file->private_data = udev;
        printk(KERN_INFO "open_bulkloop is done. \n");
	return 0;
}

static struct file_operations usb_bulkloop_fops = {
        .owner =        THIS_MODULE,
        .read =         read_bulkloop,
        .write =        write_bulkloop,
        .unlocked_ioctl = ioctl_bulkloop,
        .open =         open_bulkloop,
        .release =      close_bulkloop,
        .llseek =       noop_llseek,
};


static struct usb_class_driver usb_bulkloop_class = {
        .name =      "bulkloop",
        .fops =       &usb_bulkloop_fops,
        .minor_base =   CYSTREAM_MINOR,
};



static int
bulkloop_probe(struct usb_interface *intf, const struct usb_device_id *id)
{

        struct bulkloop_usbdev *udev = NULL;
        struct usb_host_interface *iface_desc;
        struct usb_endpoint_descriptor *endpoint;
        int retval = -ENOMEM;
        int i;
        int epin = 0; 
        int epout = 0;

        udev = kzalloc(sizeof(struct bulkloop_usbdev), GFP_KERNEL);
        if (udev == NULL) {
             dev_err(&intf->dev, "Out of memory\n");
             return -ENOMEM;
        }
        
        kref_init(&udev->kref);
        sema_init(&udev->rwsem, USB_CONCURRENT_WRITE);
        init_usb_anchor(&udev->submitted); 
        udev->cysdev = usb_get_dev(interface_to_usbdev(intf));
        udev->interface = intf;
        dev_info(&intf->dev, "USB Cystream found at address %d with %d eps.\n", udev->cysdev->devnum, 
                              intf-> cur_altsetting->desc.bNumEndpoints);

        iface_desc = intf -> cur_altsetting;
        for (i=0; i< iface_desc->desc.bNumEndpoints; i++) {
              endpoint = &iface_desc->endpoint[i].desc;

              if (usb_endpoint_is_bulk_in(endpoint)) {
                       udev->EpIn[epin].bulk_size  = usb_endpoint_maxp(endpoint);
                       udev->EpIn[epin].bulk_endpointAddr = endpoint->bEndpointAddress;

                       dev_info(&intf->dev, "USB Cystream endpoint address %ld, %2x\n", 
                                udev->EpIn[epin].bulk_size, udev->EpIn[epin].bulk_endpointAddr);

                       udev->EpIn[epin].bulk_buffer = kmalloc(udev->EpIn[epin].bulk_size, GFP_KERNEL);
                       if ( !udev->EpIn[epin].bulk_buffer) {
                              dev_err(&intf->dev,
                                      "Could not alloc bulk in buffer \n"); 
                              goto error;
                       }
                       epin ++;
              }
               
              if (usb_endpoint_is_bulk_out(endpoint)) {
  
                       udev->EpOut[epout].bulk_size  = usb_endpoint_maxp(endpoint);
                       udev->EpOut[epout].bulk_endpointAddr = endpoint->bEndpointAddress;

                       dev_info(&intf->dev, "USB Cystream endpoint address %ld, %2x\n", 
                                udev->EpOut[epout].bulk_size, udev->EpOut[epout].bulk_endpointAddr);

                       udev->EpOut[epout].bulk_buffer = kmalloc(udev->EpOut[epout].bulk_size, GFP_KERNEL);
                       if ( !udev->EpOut[epout].bulk_buffer) {
                              dev_err(&intf->dev,
                                      "Could not alloc bulk in buffer \n"); 
                              goto error;
                       }
                       epout ++;
              }


        }

	usb_set_intfdata(intf, udev);
        retval = usb_register_dev(intf, &usb_bulkloop_class);
        if (retval) {
                dev_err(&intf->dev,
                        "Not able to get a minor for this device.\n");
                goto error;
        }
	return 0;
error: 
	if(udev)
            kref_put(&udev->kref, bulkloop_cleanup);
        return retval; 
}


static int bulkloop_suspend(struct usb_interface *intf, pm_message_t message)
{
	return 0;
}

static int bulkloop_resume(struct usb_interface *intf)
{
	return 0;
}


static void bulkloop_disconnect(struct usb_interface *intf)
{
	struct bulkloop_usbdev *udev;
        int minor = intf -> minor;

        udev = usb_get_intfdata(intf);
	usb_set_intfdata(intf, NULL);
        usb_deregister_dev(intf, &usb_bulkloop_class);
        kref_put(&udev->kref, bulkloop_cleanup);
        dev_info(&intf->dev, "USB CyStream #%d now disconnected\n", minor);

}

/* fx2 version of ez-usb */
#if 0
 
static struct bulkloop_info cypress_info = {
	.name		= "CyStream device",
	.ep_in		= 6,
	.ep_out		= 2,
	.alt		= 1,
};
#endif


static const struct usb_device_id id_table[] = {

	/* generic EZ-USB FX2 controller CyStream (or development board) */
	{ USB_DEVICE(0x04b4, 0x1004),
//      	.driver_info = (unsigned long) &cypress_info,
	},
	{ }
};
MODULE_DEVICE_TABLE(usb, id_table);

static struct usb_driver bulkloop_driver = {
	.name =		"CyStream",
	.id_table =	id_table,
	.probe =	bulkloop_probe,
	.disconnect =	bulkloop_disconnect,
	.suspend =	bulkloop_suspend,
	.resume =	bulkloop_resume,
};

/*-------------------------------------------------------------------------*/

static int __init bulkloop_init(void)
{
	return usb_register(&bulkloop_driver);
}
module_init(bulkloop_init);

static void __exit bulkloop_exit(void)
{
	usb_deregister(&bulkloop_driver);
}

module_exit(bulkloop_exit);

MODULE_DESCRIPTION("USB Core/HCD CYPRESS CYSTREAM Testing Driver");
MODULE_LICENSE("GPL");

